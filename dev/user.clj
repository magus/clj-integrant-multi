(ns user
  (:require [integrant.core :as ig]
            [clojure.java.io :as io]
            inte-prof.comp.compa
            inte-prof.comp.compb
            inte-prof.comp.compbase))

;; simple, in-place configurations
(def config-base {:comp/comp-base {:foo "foo base" :bar "bar base"}})
(def config-a (merge config-base
                     {:comp/comp-a {:base (ig/ref :comp/comp-base)}}))
(def config-b (merge config-base
                     {:comp/comp-b {:base (ig/ref :comp/comp-base)}}))

(comment
  config-a
  (def sys-a (ig/init config-a))
  sys-a
  (ig/halt! sys-a)

  (def sys-b (ig/init config-b))
  sys-b
  (ig/halt! sys-b)
  )

(comment ;; reading configs from EDN files
  (def config-a0 (merge
                  (ig/read-string (slurp (io/resource "config-base.edn")))
                  (ig/read-string (slurp (io/resource "config-a.edn")))))
  config-a0
  (def sys-a0 (ig/init config-a0))
  sys-a0
  (:comp/comp-base sys-a0)
  (:comp/comp-a sys-a0)
  (:comp/comp-b sys-a0)
  (ig/halt! sys-a0)

  (def config-b0 (merge
                  (ig/read-string (slurp (io/resource "config-base.edn")))
                  (ig/read-string (slurp (io/resource "config-b.edn")))))
  config-b0
  (def sys-b0 (ig/init config-b0))
  sys-b0
  (:comp/comp-base sys-b0)
  (:comp/comp-a sys-b0)
  (:comp/comp-b sys-b0)
  (ig/halt! sys-b0)
  )
