(ns inte-prof.comp.compbase
  (:require [integrant.core :as ig]
            [java-time :as t]))

(defmethod ig/init-key :comp/comp-base [_ opts]
  (println "Starting comp-base" opts)
  (merge opts
         {:time (t/instant)}))

(defmethod ig/halt-key! :comp/comp-base [_ c]
  (println "Stopping comp-base" c)
  true)
