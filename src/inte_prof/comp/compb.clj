(ns inte-prof.comp.compb
  (:require [integrant.core :as ig]
            [java-time :as t]))

(defmethod ig/init-key :comp/comp-b [_ opts]
  (println "Starting compb" opts)
  (merge opts
         {:time (t/instant)}))

(defmethod ig/halt-key! :comp/comp-b [_ c]
  (println "Stopping compb" c)
  true)
