(ns inte-prof.comp.compa
  (:require [integrant.core :as ig]
            [java-time :as t]))

(defmethod ig/init-key :comp/comp-a [_ opts]
  (println "Starting comp-a" opts)
  (merge opts
         {:time (t/instant)}))

(defmethod ig/halt-key! :comp/comp-a [_ c]
  (println "Stopping comp-a" c)
  true)
