(ns inte-prof.main
  (:gen-class)
  (:require [clojure.tools.cli :refer [parse-opts]]))

(def ^:private cli-options
  [["-h" "--help"]])

(defn -main [& args]
  (let [{:keys [summary options errors]} (parse-opts args cli-options)]
    (cond
      errors (println errors "\n\n" summary)
      (contains? options :help) (println summary)
      :else (println "Not sure what you want, exiting!"))))
