(defproject integrant-multiprof "0.0.1-SNAPSHOT"
  :description "Experiment with multiple integrant profiles"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/tools.cli "1.0.194"]
                 [clojure.java-time "0.3.2"]

                 ;; integrant stuff
                 [integrant "0.8.0"]]
  :main ^:skip-aot inte-prof.main
  :resource-paths ["resources"]
  :profiles {:dev {:source-paths ["dev"]}
             :repl {:repl-options {:init-ns user}}})
